use crate::schema::{upload_measurements, uploads};

#[derive(Queryable, Debug)]
pub struct UploadMeasurement {
    pub id: i32,
    pub upload_id: String,
    pub repetition: i32,
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub time: i64,
}

#[derive(Insertable)]
#[table_name = "upload_measurements"]
pub struct NewUploadMeasurement {
    pub upload_id: String,
    pub repetition: i32,
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub time: i64,
}

#[derive(Insertable, Queryable)]
#[table_name = "uploads"]
pub struct UploadMetadata {
    pub upload_id: String,
    pub user_id: String,
    pub name: Option<String>,
}
