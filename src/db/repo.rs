use std::env;

use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use itertools::Itertools;
use rand::distributions::Alphanumeric;

use dotenv::dotenv;

use crate::db::models::*;
use crate::graphql::schema::{Exercise, ExerciseUpload, Measurement};
use core::borrow::Borrow;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

pub fn establish_connection_pool() -> self::Pool {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url.as_str());
    r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.")
}

pub fn delete_exercise_upload(
    conn: &PgConnection,
    up_id: String,
) -> diesel::result::QueryResult<String> {
    {
        use crate::schema::uploads::dsl::*;
        let _ = diesel::delete(uploads.filter(upload_id.eq(&up_id))).execute(conn)?;
    }
    {
        use crate::schema::upload_measurements::dsl::*;
        let _ = diesel::delete(upload_measurements.filter(upload_id.eq(&up_id))).execute(conn)?;
    }
    Ok(up_id)
}

pub fn insert_exercise_upload(conn: &PgConnection, upload: ExerciseUpload) -> String {
    use crate::schema::{upload_measurements, uploads};
    use rand::Rng;

    let upload_id = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(10)
        .collect::<String>();

    let upload_points = upload
        .repetitions
        .iter()
        .enumerate()
        .flat_map(|(rep, measurements)| {
            let repi32 = rep as i32;
            let up_id = upload_id.clone();
            measurements.iter().map(move |measurement| {
                let millis = measurement.time as i64;

                NewUploadMeasurement {
                    upload_id: up_id.clone(),
                    repetition: repi32,
                    x: measurement.x,
                    y: measurement.y,
                    z: measurement.z,
                    time: millis,
                }
            })
        })
        .collect::<Vec<NewUploadMeasurement>>();

    diesel::insert_into(upload_measurements::table)
        .values(&upload_points)
        .execute(conn)
        .expect("Error saving upload measurements");

    let upload_dto = UploadMetadata {
        upload_id: upload_id.clone(),
        name: upload.name,
        user_id: upload.user_id,
    };

    diesel::insert_into(uploads::table)
        .values(&upload_dto)
        .execute(conn)
        .expect("Error saving upload metadata");

    upload_id
}

pub fn query_exercises(conn: &PgConnection, limit: i64, offset: i64) -> Vec<Exercise> {
    use crate::schema::uploads::dsl::*;
    let metas: Vec<UploadMetadata> = uploads
        .offset(offset)
        .limit(limit)
        .load::<UploadMetadata>(conn)
        .expect("Error getting upload metadata");

    metas
        .into_iter()
        .map(|metadata| {
            let repetitions = query_upload_points(conn, metadata.upload_id.borrow());

            Exercise {
                id: metadata.upload_id,
                name: metadata.name,
                repetitions,
            }
        })
        .collect::<Vec<Exercise>>()
}

pub fn query_exercise(conn: &PgConnection, my_upload_id: &str) -> Option<Exercise> {
    let metadata = query_upload_metadata(conn, my_upload_id)?;
    let upload_points = query_upload_points(conn, my_upload_id);
    Some(Exercise {
        id: metadata.upload_id,
        name: metadata.name,
        repetitions: upload_points,
    })
}

fn query_upload_metadata(conn: &PgConnection, my_upload_id: &str) -> Option<UploadMetadata> {
    use crate::schema::uploads::dsl::*;

    uploads
        .filter(upload_id.eq(my_upload_id))
        .first::<UploadMetadata>(conn)
        .optional()
        .expect("Error getting upload metadata")
}

fn query_upload_points(conn: &PgConnection, my_upload_id: &str) -> Vec<Vec<Measurement>> {
    use crate::schema::upload_measurements::dsl::*;

    let measurements: Vec<UploadMeasurement> = upload_measurements
        .filter(upload_id.eq(my_upload_id))
        .order_by(time)
        .then_order_by(repetition)
        .load::<UploadMeasurement>(conn)
        .expect("Error loading points");

    measurements
        .iter()
        .group_by(|m| m.repetition)
        .into_iter()
        .map(|(rep, uploads)| {
            //intellij fails here
            (
                rep,
                uploads
                    .map(|upload| Measurement {
                        x: upload.x,
                        y: upload.y,
                        z: upload.z,
                        time: upload.time as f64,
                    })
                    .collect::<Vec<Measurement>>(),
            )
        })
        .map(|(_, vector)| vector) //intellij fails here
        .collect::<Vec<Vec<Measurement>>>()
}

pub fn insert(conn: &PgConnection) {
    use crate::schema::upload_measurements;

    let new_post = NewUploadMeasurement {
        upload_id: "ala".to_string(),
        repetition: 1,
        x: 1.0,
        y: 2.0,
        z: 3.0,
        time: 3i64,
    };

    diesel::insert_into(upload_measurements::table)
        .values(&new_post)
        .execute(conn)
        .expect("Error saving new post");
}

pub fn query(connection: &PgConnection) {
    use crate::schema::upload_measurements::dsl::*;

    let results = upload_measurements
        .filter(upload_id.eq("ala"))
        .limit(5)
        .load::<UploadMeasurement>(connection)
        .expect("Error loading points");

    println!("Displaying {} results", results.len());
    for post in results {
        println!("----------\n");
        println!("{:?}", post);
    }
}
