//extern crate actix_web;
#[allow(dead_code)]
#[macro_use]
extern crate diesel;
extern crate bytes;
extern crate dotenv;
extern crate itertools;
extern crate rand;

#[macro_use]
extern crate serde_derive;

extern crate juniper;

#[macro_use]
extern crate juniper_codegen;

pub mod graphql {
    pub mod schema;
    pub mod server;
}

pub mod db {
    pub mod models;
    pub mod repo;
}

pub mod schema;

fn main() {
    let ala = graphql::server::server();
    ala.unwrap();
    //    let connection = db::repo::establish_connection();
    //    db::repo::insert(&connection);
    //    db::repo::query(&connection);
}
