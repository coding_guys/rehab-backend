use juniper::FieldResult;
use juniper::RootNode;

use crate::db::repo::Pool;

#[derive(GraphQLInputObject)]
#[graphql(description = "MeasurementInput from accelerometer")]
pub struct MeasurementInput {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub time: f64, //we don't have i64?!
}

#[derive(GraphQLObject)]
#[graphql(description = "Measurement from accelerometer")]
pub struct Measurement {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub time: f64, //we don't have i64?!
}

#[derive(GraphQLInputObject)]
#[graphql(description = "for now we provide userid here :)")]
pub struct ExerciseUpload {
    pub user_id: String,
    pub name: Option<String>,
    pub repetitions: Vec<Vec<MeasurementInput>>,
}

#[derive(GraphQLObject)]
#[graphql]
pub struct Exercise {
    pub id: String, //upload ID
    pub name: Option<String>,
    pub repetitions: Vec<Vec<Measurement>>,
}

pub struct Context {
    pub pool: Pool,
}

impl juniper::Context for Context {}

pub struct Query {}

#[juniper_codegen::object(Context = Context)]
impl Query {
    fn api_version() -> &str {
        "0.1"
    }

    // Arguments to resolvers can either be simple types or input objects.
    // To gain access to the context, we specify a argument
    // that is a reference to the Context type.
    // Juniper automatically injects the correct context here.
    fn exercise(context: &Context, upload_id: String) -> FieldResult<Option<Exercise>> {
        let conn = &context.pool.get().unwrap();
        Ok(crate::db::repo::query_exercise(conn, &upload_id))
    }

    fn exercises(context: &Context, limit: i32, offset: i32) -> FieldResult<Vec<Exercise>> {
        let conn = &context.pool.get().unwrap();
        Ok(crate::db::repo::query_exercises(
            conn,
            limit as i64,
            offset as i64,
        ))
    }
}

pub struct Mutation;

#[juniper_codegen::object(Context = Context)]
impl Mutation {
    fn upload_exercise(context: &Context, exercise: ExerciseUpload) -> String {
        let conn = &context.pool.get().unwrap();
        crate::db::repo::insert_exercise_upload(conn, exercise)
    }

    fn delete_exercise(context: &Context, upload_id: String) -> String {
        let conn = &context.pool.get().unwrap();
        let result_id = crate::db::repo::delete_exercise_upload(conn, upload_id).unwrap();
        result_id
    }
}

pub type Schema = RootNode<'static, Query, Mutation>;

pub fn create_schema() -> Schema {
    Schema::new(Query {}, Mutation {})
}
