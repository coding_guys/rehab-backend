use std::io;
use std::sync::Arc;

use actix_web::*;
use futures::future::Future;
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;

use crate::graphql::schema::{create_schema, Context, Mutation, Query, Schema};
use actix_web::web::Json;

fn graphiql() -> HttpResponse {
    let html = graphiql_source("http://127.0.0.1:8080/graphql");
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

fn graphql(
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        println!("Creating endpoint");
        let pool = crate::db::repo::establish_connection_pool();

        let ctx = Context { pool };
        let res = data.execute::<Context, Query, Mutation>(&st, &ctx);
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .map_err(Error::from)
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}

#[derive(Deserialize)]
struct Message {
    text: String,
}

fn test(msg: Json<Message>) -> HttpResponse {
    let size = msg.text.bytes().len();

    HttpResponse::Ok()
        .content_type("text/plain; charset=utf-8")
        .body(format!("Text len is {}", size))
}

pub fn server() -> io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    // Create Juniper schema
    let schema = std::sync::Arc::new(create_schema());

    // Start http server
    HttpServer::new(move || {
        App::new()
            .data(web::JsonConfig::default().limit(4096 * 1024 * 1024)) // <- limit size of the payload (global configuration)
            .data(schema.clone())
            .wrap(middleware::Logger::default())
            .service(web::resource("/graphql").route(web::post().to_async(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
            .service(web::resource("/test").route(web::post().to(test)))
    })
    .bind("127.0.0.1:8080")?
    .run()
}
