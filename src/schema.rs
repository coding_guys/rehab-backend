table! {
    upload_measurements (id) {
        id -> Int4,
        upload_id -> Varchar,
        repetition -> Int4,
        x -> Float8,
        y -> Float8,
        z -> Float8,
        time -> Int8,
    }
}

table! {
    uploads (upload_id) {
        upload_id -> Varchar,
        user_id -> Varchar,
        name -> Nullable<Varchar>,
    }
}

allow_tables_to_appear_in_same_query!(upload_measurements, uploads,);
