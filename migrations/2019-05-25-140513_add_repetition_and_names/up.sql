CREATE TABLE upload_measurements (
  id SERIAL PRIMARY KEY,
  upload_id VARCHAR NOT NULL,
  repetition INT NOT NULL ,
  x DOUBLE PRECISION NOT NULL,
  y DOUBLE PRECISION NOT NULL,
  z DOUBLE PRECISION NOT NULL,
  time BIGINT Not NULL
);

create table uploads (
    upload_id VARCHAR NOT NULL PRIMARY KEY,
    user_id VARCHAR NOT NULL,
    name VARCHAR
);
