network:
	 docker network create --subnet=172.21.0.0/16 rehabbackend_default  

up: 
	docker-compose -f docker_services.yaml up -d

down:
	docker-compose -f docker_services.yaml down

restart:
	docker-compose -f docker_services.yaml down && docker-compose -f docker_services.yaml up
