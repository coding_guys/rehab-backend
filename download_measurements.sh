#!/bin/bash

dir_name=$(pwd|awk -F "/" '{print $NF}')
without_dash=$(echo $dir_name| tr -d '-')
pg_name="${without_dash}_db_1"

function psql_execute {
    docker exec $pg_name /usr/bin/psql -d rehab -U username -c "$1"
}

export -f psql_execute
export  psql_execute
psql_execute "\copy upload_measurements to '/upload_measurements.csv' csv"
docker cp $pg_name:/upload_measurements.csv .
psql_execute "select * from upload_measurements limit 1" | head -n1 | \
    sed 's/\ *|\ */,/g' > ala.csv

cat upload_measurements.csv | tr -d '\t' | tr -d ' ' >> ala.csv 
mv ala.csv upload_measurements.csv

